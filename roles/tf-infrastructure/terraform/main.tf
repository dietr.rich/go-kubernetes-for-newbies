provider "hcloud" {
  token = var.hcloud_token
}

# private network and subnets

resource "hcloud_network" "kubernetes" {
  name     = var.network_name
  ip_range = var.network_ip_range
  labels   = { "cluster-membership" = var.cluster_name }
}

resource "hcloud_network_subnet" "k8s-master-net" {
  network_id   = hcloud_network.kubernetes.id
  type         = "server"
  network_zone = "eu-central"
  ip_range     = var.subnet_master_ip_range
}

resource "hcloud_network_subnet" "k8s-worker-net" {
  network_id   = hcloud_network.kubernetes.id
  type         = "server"
  network_zone = "eu-central"
  ip_range     = var.subnet_worker_ip_range
}

# Master server

resource "hcloud_server" "master" {
  count       = 1
  name        = "master"
  image       = var.os_image
  server_type = var.master_server_type
  location    = var.datacenter
  user_data   = file("./user-data/cloud-config.yaml")
  labels      = { "node-role" = "master", "cluster-membership" = var.cluster_name }
}

resource "hcloud_rdns" "rdns_master" {
  count      = length(hcloud_server.master)
  server_id  = hcloud_server.master[count.index].id
  ip_address = hcloud_server.master[count.index].ipv4_address
  dns_ptr    = "master.${var.domain}"
}

resource "hcloud_server_network" "master_network" {
  count      = length(hcloud_server.master)
  subnet_id  = hcloud_network_subnet.k8s-master-net.id
  server_id  = hcloud_server.master.*.id[count.index]
  ip         = "${var.subnet_master_ip}.${count.index + 2}"
}

# Worker servers

resource "hcloud_server" "worker" {
  count       = var.worker_count
  name        = "node-${count.index + 1}"
  image       = var.os_image
  server_type = var.worker_server_type
  location    = var.datacenter
  user_data   = file("./user-data/cloud-config.yaml")
  labels      = { "node-role" = "worker", "cluster-membership" = var.cluster_name }
}

resource "hcloud_rdns" "rdns_worker" {
  count      = length(hcloud_server.worker)
  server_id  = hcloud_server.worker[count.index].id
  ip_address = hcloud_server.worker[count.index].ipv4_address
  dns_ptr    = "node-${count.index + 1}.${var.domain}"
}

resource "hcloud_server_network" "worker_network" {
  count      = length(hcloud_server.worker)
  subnet_id  = hcloud_network_subnet.k8s-worker-net.id
  server_id  = hcloud_server.worker.*.id[count.index]
  ip         = "${var.subnet_worker_ip}.${count.index + 1}"
}

# Floating IP

resource "hcloud_floating_ip" "lbipv4" {
  count         = var.floatip_count
  type          = "ipv4"
  home_location = var.datacenter
  name          = var.floatip_count == 1 ? "lb.${var.domain}" : "lb-${count.index + 1}.${var.domain}"
}

resource "hcloud_rdns" "rdns_lbipv4" {
  count          = length(hcloud_floating_ip.lbipv4)
  floating_ip_id = hcloud_floating_ip.lbipv4[count.index].id
  ip_address     = hcloud_floating_ip.lbipv4[count.index].ip_address
  dns_ptr        = length(hcloud_floating_ip.lbipv4) == 1 ? "lb.${var.domain}" : "lb-${count.index + 1}.${var.domain}"
}
